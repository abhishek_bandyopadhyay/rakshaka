package com.example.android.rakshaka.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.rakshaka.R;
import com.example.android.rakshaka.adapters.NavigationDrawerListAdapter;
import com.example.android.rakshaka.listener.AllP2PListeners;
import com.example.android.rakshaka.listener.P2PConnectionListener;
import com.example.android.rakshaka.receivers.RakshakaP2PReceiver;

import java.util.ArrayList;

/**
 * Created by Abhishek on 04/09/15.
 */
public class RakshakaP2PMonitoringActivity extends ActionBarActivity implements ListView.OnItemClickListener,View.OnClickListener{

    private WifiP2pManager p2pManager;
    private Channel p2pChannel;
    private RakshakaP2PReceiver p2pReceiver;
    private IntentFilter p2pEventsFilter;
    private WifiP2pConfig config;
    private WifiP2pDevice device;
    public ArrayList<WifiP2pDevice> peers;
    private AllP2PListeners allP2PListeners;
    public ListView deviceListView;
    private ActionBarDrawerToggle navigationDrawerToggle;
    private DrawerLayout navigationDrawer;
    private ListView navigationItemsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peer_monitor);
        deviceListView = (ListView) findViewById(R.id.device_list);
        deviceListView.setOnItemClickListener(this);
        setUpNavigationDrawer();
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.custom_action_bar_layout);
        actionBar.getCustomView().findViewById(R.id.menu_icon).setOnClickListener(this);
        ((TextView)actionBar.getCustomView().findViewById(R.id.action_bar_title)).setText(getString(R.string.find_peer));
        actionBar.show();

        p2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        p2pChannel = p2pManager.initialize(this, getMainLooper(), null);
        p2pReceiver = new RakshakaP2PReceiver(p2pManager, p2pChannel, this);
        p2pEventsFilter = new IntentFilter();
        p2pEventsFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        p2pEventsFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        p2pEventsFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        p2pEventsFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        allP2PListeners = new AllP2PListeners(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        p2pManager.discoverPeers(p2pChannel, allP2PListeners);

//        registerReceiver(p2pReceiver, p2pEventsFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        unregisterReceiver(p2pReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        p2pManager.removeGroup(p2pChannel, allP2PListeners);
        p2pManager.cancelConnect(p2pChannel, allP2PListeners);
    }

    public void findPeer(View v){
        p2pManager.initialize(this, Looper.getMainLooper(), allP2PListeners);
        p2pManager.requestPeers(p2pChannel, allP2PListeners);
    }
    public void removePeer(View v){

        p2pManager.cancelConnect(p2pChannel, allP2PListeners);
        p2pManager.removeGroup(p2pChannel, allP2PListeners);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(peers.size()!=0) {
            device = peers.get(position);
            config = new WifiP2pConfig();
            config.deviceAddress = device.deviceAddress;
            config.wps.setup = WpsInfo.PBC;
            p2pManager.connect(p2pChannel,config,new P2PConnectionListener(this));
            p2pManager.requestConnectionInfo(p2pChannel,new P2PConnectionListener(this));
        }
    }

    private void setUpNavigationDrawer() {

        navigationDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationItemsListView = (ListView) findViewById(R.id.list_slidermenu);
        navigationItemsListView.setAdapter(new NavigationDrawerListAdapter(this));
        navigationItemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = null;

                switch (position){
                    case 0: intent = new Intent(RakshakaP2PMonitoringActivity.this, DisasterDetailedActivity.class);
                            intent.putExtra("id","natural");
                            break;

                    case 1: intent = new Intent(RakshakaP2PMonitoringActivity.this, DisasterDetailedActivity.class);
                            intent.putExtra("id","manmade");
                            break;

                    case 2: navigationDrawer.closeDrawer(navigationItemsListView);
                            break;

                    case 3: intent = new Intent(RakshakaP2PMonitoringActivity.this, UserDetailsActivity.class);
                            break;

                    case 4: //TODO Implement later
                            break;
                }

                if(intent != null){
                    startActivity(intent);
                }
            }
        });

        navigationDrawerToggle = new ActionBarDrawerToggle(this, navigationDrawer,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                navigationDrawer.closeDrawer(navigationItemsListView);
            }

            public void onDrawerOpened(View drawerView) {
                navigationDrawer.openDrawer(navigationItemsListView);
            }
        };
        navigationDrawer.setDrawerListener(navigationDrawerToggle);
    }

    @Override
    public void onClick(View v) {
        if(navigationDrawer.isDrawerOpen(navigationItemsListView)){
            navigationDrawer.closeDrawer(navigationItemsListView);
        }else {
            navigationDrawer.openDrawer(navigationItemsListView);
        }
    }
}
