package com.example.android.rakshaka.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;

import com.example.android.rakshaka.activity.RakshakaP2PMonitoringActivity;
import com.example.android.rakshaka.utils.ToastHelper;

/**
 * Created by Abhishek on 04/09/15.
 */
public class RakshakaP2PReceiver extends BroadcastReceiver {

    private WifiP2pManager p2pManager;
    private Channel p2pChannel;
    private RakshakaP2PMonitoringActivity rakshakaP2PMonitoringActivity;

    public RakshakaP2PReceiver(WifiP2pManager manager, Channel channel, RakshakaP2PMonitoringActivity activity) {
        super();
        p2pManager = manager;
        p2pChannel = channel;
        rakshakaP2PMonitoringActivity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                // Wifi P2P is enabled
                ToastHelper.showToast(context,"p2p enabled in receiver");
            } else {
                // Wi-Fi P2P is not enabled
                ToastHelper.showToast(context,"p2p not enabled in receiver");
            }
        }

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            ToastHelper.showToast(context,"p2p enabled and notify activity in receiver");

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            // Call WifiP2pManager.requestPeers() to get a list of current peers
            ToastHelper.showToast(context,"peer got changed in receiver");

        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            // Respond to new connection or disconnections
            ToastHelper.showToast(context,"p2p connection changed in receiver");

        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // Respond to this device's wifi state changing
            ToastHelper.showToast(context,"Device changed in receiver");
        }
    }
}
