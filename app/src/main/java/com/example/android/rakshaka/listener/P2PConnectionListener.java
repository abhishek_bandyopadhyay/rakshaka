package com.example.android.rakshaka.listener;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;

import com.example.android.rakshaka.utils.ToastHelper;

/**
 * Created by Abhishek on 05/09/15.
 */
public class P2PConnectionListener implements WifiP2pManager.ActionListener,WifiP2pManager.ConnectionInfoListener {

    private Context context;

    public P2PConnectionListener(Context context){
        this.context = context;
    }

    @Override
    public void onSuccess() {
        ToastHelper.showToast(context, "Peer Connection Succeded");
    }

    @Override
    public void onFailure(int reason) {
        ToastHelper.showToast(context,"Peer connection failed" + String.valueOf(reason));
    }

    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo info) {
        ToastHelper.showToast(context,info.toString());
    }
}
