package com.example.android.rakshaka.listener;

import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.ChannelListener;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;

import com.example.android.rakshaka.activity.RakshakaP2PMonitoringActivity;
import com.example.android.rakshaka.adapters.DeviceListAdapter;
import com.example.android.rakshaka.utils.ToastHelper;

import java.util.ArrayList;

/**
 * Created by Abhishek on 04/09/15.
 */
public class AllP2PListeners implements ActionListener,ChannelListener,PeerListListener {

    private RakshakaP2PMonitoringActivity activity;
    private ArrayList<WifiP2pDevice> peerList = new ArrayList<>();
    private WifiP2pConfig config;
    private WifiP2pDevice device;

    public AllP2PListeners(RakshakaP2PMonitoringActivity activity){
        this.activity = activity;
    }

    @Override
    public void onSuccess() {
        ToastHelper.showToast(activity, "peer discovered in Activity");
    }

    @Override
    public void onFailure(int reason) {
        ToastHelper.showToast(activity, "Failed to find peer in Activity" + String.valueOf(reason));
    }

    @Override
    public void onChannelDisconnected() {
        ToastHelper.showToast(activity, "peer got disconnected in Activity");
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peers) {

        peerList.clear();
        peerList.addAll(peers.getDeviceList());
        if (peerList.size() == 0) {
            ToastHelper.showToast(activity, "No peer available right now");
        } else{
            DeviceListAdapter deviceListAdapter = new DeviceListAdapter(activity,peerList);
            deviceListAdapter.notifyDataSetChanged();
            activity.deviceListView.setAdapter(deviceListAdapter);
            activity.peers = peerList;
        }
    }
}
