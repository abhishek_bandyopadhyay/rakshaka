package com.example.android.rakshaka.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.example.android.rakshaka.R;

public class WeatherActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
    }
}
