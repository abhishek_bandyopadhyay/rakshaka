package com.example.android.rakshaka.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Abhishek on 04/09/15.
 */
public class ToastHelper {

    public static void showToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }
}
