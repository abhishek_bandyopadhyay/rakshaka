package com.example.android.rakshaka.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.android.rakshaka.R;

import java.util.ArrayList;
import java.util.List;

public class DisasterDetailedActivity extends ActionBarActivity {

    static private ArrayAdapter<String> mForecastAdapter;
    List<String> subDisastersList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disaster_detailed);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        String id = getIntent().getStringExtra("id");

        if(id != null) {
            if (id.equals("natural")) {
                subDisastersList.add("EarthQuake");
                subDisastersList.add("Flood");
                subDisastersList.add("LandSlide");
                subDisastersList.add("Tornado");
                subDisastersList.add("Tsunami");
            } else if (id.equals("manmade")) {
                subDisastersList.add("Accident");
                subDisastersList.add("Baby Choking");
                subDisastersList.add("Bike Accident");
                subDisastersList.add("Hazardous");
            }
        }

        mForecastAdapter = new ArrayAdapter<String>(this,R.layout.list_item_disaster,R.id.list_item_textview,subDisastersList);
        final ListView listView = (ListView) findViewById(R.id.listview_disasters);
        listView.setAdapter(mForecastAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String clickedItem = listView.getItemAtPosition(position).toString();
                Intent intent = new Intent(DisasterDetailedActivity.this,SliderActivity.class);
                intent.putExtra("clickedItem",clickedItem);
                intent.putExtra("disaster_type",id);
                startActivity(intent);
            }
        });
    }
}
