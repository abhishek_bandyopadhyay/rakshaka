package com.example.android.rakshaka.adapters;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.android.rakshaka.R;

import java.util.ArrayList;

/**
 * Created by Abhishek on 05/09/15.
 */
public class DeviceListAdapter extends BaseAdapter {

    private Context context;
    private TextView deviceName;
    private ArrayList<WifiP2pDevice> deviceNames;

    public DeviceListAdapter(Context context, ArrayList<WifiP2pDevice> deviceNames){
        this.context = context;
        this.deviceNames = deviceNames;
    }

    @Override
    public int getCount() {
        return deviceNames.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.device_list_individual_item,parent,false);
        }
        deviceName = (TextView) convertView.findViewById(R.id.device_name);
        deviceName.setText(deviceNames.get(position).deviceName);
        return convertView;
    }
}
