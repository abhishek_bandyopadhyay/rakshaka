package com.example.android.rakshaka.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.android.rakshaka.R;

import java.util.LinkedList;

/**
 * Created by ritesh on 9/5/15.
 */
public class CustomSwipeAdapter extends PagerAdapter {

    private LinkedList<Integer> image_resources;

    private Context ctx;
    private LayoutInflater layoutInflater;
    private String clickedItem;

    public CustomSwipeAdapter(Context ctx,String clickedItem){
        this.ctx = ctx;
        this.clickedItem = clickedItem;

        image_resources = new LinkedList<Integer>();

        switch (clickedItem){
            case "EarthQuake":
                image_resources.add(R.drawable.earthquake_one);
                image_resources.add(R.drawable.earthquake_two);
                image_resources.add(R.drawable.earthquake_three);
                image_resources.add(R.drawable.earthquake_four);
                break;
            case "Flood":
                image_resources.add(R.drawable.flood_one);
                image_resources.add(R.drawable.flood_two);
                image_resources.add(R.drawable.flood_three);
                image_resources.add(R.drawable.flood_four);
                image_resources.add(R.drawable.flood_five);
                break;
            case "LandSlide":
                image_resources.add(R.drawable.landslide_one);
                image_resources.add(R.drawable.landslide_two);
                image_resources.add(R.drawable.landslide_three);
                image_resources.add(R.drawable.landslide_four);
                image_resources.add(R.drawable.landslide_five);
                break;
            case "Tornado":
                image_resources.add(R.drawable.tornado_one);
                image_resources.add(R.drawable.tornado_two);
                image_resources.add(R.drawable.tornado_three);
                image_resources.add(R.drawable.tornado_four);
                image_resources.add(R.drawable.tornado_five);
                image_resources.add(R.drawable.tornado_six);
                break;
            case "Tsunami":
                image_resources.add(R.drawable.tsunami_one);
                image_resources.add(R.drawable.tsunami_two);
                image_resources.add(R.drawable.tsunami_three);
                image_resources.add(R.drawable.tsunami_four);
                image_resources.add(R.drawable.tsunami_five);
                image_resources.add(R.drawable.tsunami_six);
                break;
            case "Accident":
                image_resources.add(R.drawable.accident_one);
                image_resources.add(R.drawable.accident_two);
                image_resources.add(R.drawable.acciden_three);
                image_resources.add(R.drawable.accident_four);
                image_resources.add(R.drawable.accident_five);
                break;
            case "Baby Choking":
                image_resources.add(R.drawable.baby_one);
                image_resources.add(R.drawable.baby_two);
                image_resources.add(R.drawable.baby_three);
                image_resources.add(R.drawable.baby_four);
                image_resources.add(R.drawable.baby_five);
                break;
            case "Bike Accident":
                image_resources.add(R.drawable.bike_one);
                image_resources.add(R.drawable.bike_two);
                image_resources.add(R.drawable.bike_three);
                break;
            case "Hazardous":
                image_resources.add(R.drawable.hazardious_one);
                image_resources.add(R.drawable.hazardious_two);
                image_resources.add(R.drawable.hazardious_three);
                break;
        }
    }

    @Override
    public int getCount() {
        return image_resources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = layoutInflater.inflate(R.layout.swipe_layout,container,false);
//        ImageView imageView = (ImageView) item_view.findViewById(R.id.image_view_pager);
//        imageView.setImageResource(image_resources.get(position));
        LinearLayout linearLayout = (LinearLayout) item_view.findViewById(R.id.swipe_layout_id);
        linearLayout.setBackgroundResource(image_resources.get(position));
        container.addView(item_view);
        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
