package com.example.android.rakshaka.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

import com.example.android.rakshaka.R;
import com.example.android.rakshaka.adapters.CustomSwipeAdapter;

public class SliderActivity extends ActionBarActivity {

    ViewPager viewPager;
    CustomSwipeAdapter adapter;
    private String currentDisasterType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String clickedItem =  intent.getStringExtra("clickedItem");
        currentDisasterType = intent.getStringExtra("disaster_type");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setContentView(R.layout.activity_slider);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        adapter = new CustomSwipeAdapter(this,clickedItem);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,DisasterDetailedActivity.class);
        intent.putExtra("id",currentDisasterType);
        finish();
    }
}
