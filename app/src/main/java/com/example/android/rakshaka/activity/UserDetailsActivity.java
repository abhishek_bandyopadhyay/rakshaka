package com.example.android.rakshaka.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.android.rakshaka.R;


public class UserDetailsActivity extends ActionBarActivity {

    private EditText userNameEt,contactNumberEt,emergencyContactNumberEt,addressEt;
    private Spinner bloodGroupSpinner;
    private String username,contactNumber,emergencyContactNumber,bloodGroup,address;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static String USERNAME_KEY = "username";
    private static String CONTACT_NO_KEY = "contact_no";
    private static String EMERGENCY_NO_KEY = "emergency_no";
    private static String ADDRESS_KEY = "address";
    private static String BLOOD_GROUP_KEY = "blood_group";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        preferences = getSharedPreferences("rakshaka_preferences", MODE_PRIVATE);
        setUpInputField();
    }

    public void onSubmit(View v){
        getValuesOfTheInputFields();

        if(areValidContactNumbers()){
            editor = preferences.edit();
            editor.putString(USERNAME_KEY,username);
            editor.putString(CONTACT_NO_KEY,contactNumber);
            editor.putString(EMERGENCY_NO_KEY,emergencyContactNumber);
            editor.putString(ADDRESS_KEY, address);
            if(!bloodGroup.equals("Select Blood Group")){
               editor.putString(BLOOD_GROUP_KEY,bloodGroup);
            }
            editor.apply();
            startActivity(new Intent(this,RakshakaP2PMonitoringActivity.class));
            finish();
        }
    }

    private void setUpInputField(){
        userNameEt = (EditText) findViewById(R.id.username);
        contactNumberEt = (EditText) findViewById(R.id.contact_number);
        emergencyContactNumberEt = (EditText) findViewById(R.id.emergency_number);
        addressEt = (EditText) findViewById(R.id.address);
        bloodGroupSpinner = (Spinner) findViewById(R.id.blood_group_spinner);

        userNameEt.setText(preferences.getString(USERNAME_KEY,""));
        contactNumberEt.setText(preferences.getString(CONTACT_NO_KEY,""));
        emergencyContactNumberEt.setText(preferences.getString(EMERGENCY_NO_KEY,""));
        addressEt.setText(preferences.getString(ADDRESS_KEY,""));
        String allBloodGroups[] = getResources().getStringArray(R.array.blood_groups);
        for(int i=0; i<allBloodGroups.length; i++){
            if(allBloodGroups[i].equals(preferences.getString(BLOOD_GROUP_KEY,""))){
                bloodGroupSpinner.setSelection(i);
            }
        }
    }

    private void getValuesOfTheInputFields(){
        username = userNameEt.getText().toString();
        contactNumber = contactNumberEt.getText().toString();
        emergencyContactNumber = emergencyContactNumberEt.getText().toString();
        bloodGroup = bloodGroupSpinner.getSelectedItem().toString();
        address = addressEt.getText().toString();
    }

    private boolean areValidContactNumbers(){

        if(!contactNumber.equals("") && contactNumber.length() < 10){
            contactNumberEt.setError(getString(R.string.invalid_phone_number));
            return false;
        }else if(!emergencyContactNumber.equals("") && emergencyContactNumber.length() < 10){
            emergencyContactNumberEt.setError(getString(R.string.invalid_phone_number));
            return false;
        }else{
            return true;
        }
    }

}
