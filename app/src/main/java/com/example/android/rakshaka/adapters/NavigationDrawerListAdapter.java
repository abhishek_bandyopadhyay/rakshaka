package com.example.android.rakshaka.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.android.rakshaka.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Abhishek on 05/09/15.
 */
public class NavigationDrawerListAdapter extends BaseAdapter {

    private List<String> navigationOptions;
    private Context context;
    private TextView navigationItem;

    public NavigationDrawerListAdapter(Context context){
        this.context = context;
        navigationOptions = Arrays.asList(context.getResources().getStringArray(R.array.nav_drawer_items));
    }

    @Override
    public int getCount() {
        return navigationOptions.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.navigation_individual_list_item,parent,false);
        }
        navigationItem = (TextView) convertView.findViewById(R.id.individual_nav_item);
        navigationItem.setText(navigationOptions.get(position));

        return convertView;
    }
}
