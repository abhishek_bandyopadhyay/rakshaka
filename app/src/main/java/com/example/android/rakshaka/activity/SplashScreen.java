package com.example.android.rakshaka.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.example.android.rakshaka.R;

/**
 * Created by ritesh on 9/4/15.
 */
public class SplashScreen extends Activity {

    private static int SPLASH_TIME_OUT = 3000;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        preferences = getSharedPreferences("rakshaka_preferences",MODE_PRIVATE);

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {

                Intent intent;
                if(preferences.getBoolean("first_time_in_the_app",true)) {
                     intent = new Intent(SplashScreen.this, UserDetailsActivity.class);
                     editor = preferences.edit();
                     editor.putBoolean("first_time_in_the_app",false);
                     editor.apply();
                }else{
                    intent = new Intent(SplashScreen.this,RakshakaP2PMonitoringActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
